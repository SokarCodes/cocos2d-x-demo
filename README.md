# That "Space shooter" -game! #

That "Space shooter" -game! is my latest leisure time creation. For the first time I am using Cocos2D-x game engine with C++ and I learned to love it through hating it! Yes, there were some frustrating obstacles past two weeks I did this but I overcame most of them. It was fun but it is not done! I have big plans for this game! BIG! :D

## Download binaries ##

See repository download pages for Android and Win32 binaries.

https://bitbucket.org/Juginabi/cocos2d-x-demo/downloads

## Issues ##

Issue list is long. Some of the issues are marked to Jira but from the top of my head things which are lacking as of 26.8.2016 are:

* Android high score saving not working (EDIT: It works now as of 30.8.2016). Works on Win32.
* There are run-time memory allocations mainly from particle generation. Need controller factory class for those.
* This is done in two weeks... in a rush! Lots of refactoring to be done!

## License ##
This software is distributed under Apache v2.0 licence for now. We will see if better ones are necessary in the future.

## Video trailer (technical somewhat) ##

You can watch my short trailer from here. It is 100% captured from my android phone (OnePlus3):

* https://www.youtube.com/watch?v=tC1lO1my2DY

This trailer is captured from win32 version using geforce experience.

* https://www.youtube.com/watch?v=xtrz3zHoEz8

## Screenshots ##

Check out some screenshots! These are from win32 build.

### 30.8.2016 ###

![Screenshot_20160829-235202.png](https://bitbucket.org/repo/jLp74a/images/2305038600-Screenshot_20160829-235202.png)

Updated menu items and fonts. I am using Kenney.nl fonts and graphics.

### Various alpha version pictures ###
![FourTerrains.png](https://bitbucket.org/repo/jLp74a/images/417023423-FourTerrains.png)

![MainMenuScreen.png](https://bitbucket.org/repo/jLp74a/images/3102440858-MainMenuScreen.png)

![HighScore.png](https://bitbucket.org/repo/jLp74a/images/4249865254-HighScore.png)

![HighScore2.png](https://bitbucket.org/repo/jLp74a/images/2201982597-HighScore2.png)

![Explosion.png](https://bitbucket.org/repo/jLp74a/images/3822172751-Explosion.png)